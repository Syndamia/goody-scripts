# goody-scripts
Adds userscript support to the badwolf browser.

This is a fork of [ephy-scripts](https://github.com/rliang/ephy-scripts) by
[rliang](https://github.com/rliang), which updates it to work with badwolf,
instead of Epiphany (Gnome Web).

# What
This is a WebKit2GTK WebExtension which simply reads Javascript files from some
directories, and evaluates them for every page.

Being a WebExtension, it is isolated from the GTK UI and thus can't modify it.

# How
Just place your scripts under either:

* `~/.local/share/epiphany/userscripts` *(Recommended)*
* `/usr/local/share/badwolf/userscripts`
* `/usr/share/badwolf/userscripts`

# Requirements
* `pkg-config`
* `libwebkit2gtk-4.0-dev` or `webkit2gtk-web-extension-4.0`

# Installation

```sh
make && make install
```

# License
GPL3
